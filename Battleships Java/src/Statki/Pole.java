package Statki;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

class Pole extends JFrame implements ActionListener {

    JButton[][] array2dtop = new JButton[10][10];

    Pole(String title) {

        super(title);
        setLayout(new GridLayout(10, 10, 5, 5));
        for (int x = 0; x < array2dtop.length; x++) {
            for (int y = 0; y < array2dtop.length; y++) {
                array2dtop[x][y] = new JButton(String.valueOf((x * 10) + y));
                array2dtop[x][y].setActionCommand(String.valueOf(3));
                array2dtop[x][y].addActionListener(this);
                // Force buttons to be square.
                Dimension dim = array2dtop[x][y].getPreferredSize();
                int buttonSize = Math.max(dim.height, dim.width);
                array2dtop[x][y].setPreferredSize(new Dimension(buttonSize, buttonSize));

                add(array2dtop[x][y]);
            }
        }
    }

    public void actionPerformed1(ActionEvent evt) {

        int cmd = Integer.parseInt(evt.getActionCommand());
        if (cmd == 3) {
            System.out.println(3);
        }
    }

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}




    public static void main(String[] args) {

        Pole board = new Pole("BattleShip");
        board.pack();
        board.setLocationRelativeTo(null);
        board.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        board.setVisible(true);
    }
}