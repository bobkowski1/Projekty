package Statki;

import java.awt.BorderLayout;
import java.util.Random;
import java.util.Scanner;

import Statki.Okrety;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.util.Arrays;
import java.util.Properties;

public class Plansza extends JFrame implements ActionListener, MouseListener, 
MouseMotionListener, ChangeListener {

	private JButton[][] Pole = new JButton[11][11];
	private JButton[][] Pole2 = new JButton[11][11];
	private JButton NowaGra; private JButton Rozstaw; private JButton Zapisz;
	private JButton Wczytaj; private JButton Zwycięstwo; private JButton Porażka;
	private JButton Prawo,Gora; private JButton Okret4,Okret3,Okret2,Okret1;
	private JPanel menu; private JPanel przeciwnik; private JPanel własne; private JPanel Centrum;
	protected static int FazaGry;
	private int check1,check2,check3,check4,check5,check6;
	private int NumerStatku;
	private int NumerStatkuPrzeciwnik;
	private int Kierunek;
	private int KierunekPrzeciwnik;
	private int Następny=1;
	public int Maks1 = 0;
	private static int Statki;
	public boolean[][] ObecnoscStatku2 = new boolean[10][10];
	public boolean[][] StrzałOddany = new boolean[10][10];
	public boolean[][] StrzałOddanyPrzeciwnik= new boolean[10][10];
	public int StrzałTrafiony=0;
	public boolean CzyStrzałOddany=false;
	Okrety X = new Okrety();
	public Plansza(int width, int height) {
		this.setLayout(new BorderLayout());
		this.menu = new JPanel();
		this.menu.setLayout(new FlowLayout());
		this.menu.setBackground(Color.DARK_GRAY);
		this.NowaGra = new JButton("Nowa Gra");
		this.Wczytaj = new JButton("Wczytaj");
		this.Zapisz = new JButton("Zapisz");
		this.Rozstaw = new JButton("Rozstaw Statki");
		this.Okret1 = new JButton(new ImageIcon("C:\\Users\\Dom\\eclipse-workspace\\Statki\\src\\Okret1.png"));
		this.Okret2 = new JButton(new ImageIcon("C:\\\\Users\\\\Dom\\eclipse-workspace\\Statki\\src\\Okret2.png"));
		this.Okret3 = new JButton(new ImageIcon("C:\\Users\\Dom\\eclipse-workspace\\Statki\\src\\Okret3.png"));
		this.Okret4 = new JButton(new ImageIcon("C:\\Users\\Dom\\eclipse-workspace\\Statki\\src\\Okret4.png"));
		this.Prawo = new JButton(new ImageIcon("C:\\Users\\Dom\\eclipse-workspace\\Statki\\src\\Prawo.png"));
		this.Gora = new JButton(new ImageIcon("C:\\Users\\Dom\\eclipse-workspace\\Statki\\src\\Gora.png"));
		this.Zwycięstwo = new JButton(new ImageIcon("C:\\Users\\Dom\\eclipse-workspace\\Statki\\src\\Zwycięstwo.png"));
		this.Porażka = new JButton(new ImageIcon("C:\\Users\\Dom\\eclipse-workspace\\Statki\\src\\Porażka.png"));
		setVisible(true);
		this.add(menu,BorderLayout.NORTH);
		this.Centrum = new JPanel();
		this.przeciwnik = new JPanel();
		this.przeciwnik.setLayout(new GridLayout(10,10));
		przeciwnik.setPreferredSize(new Dimension(450, 350));
		this.własne = new JPanel();
		this.menu.add(this.NowaGra);
		this.menu.add(this.Wczytaj);
		this.Rozstaw.addActionListener(this);
		this.NowaGra.addActionListener(this);
		this.Zapisz.addActionListener(this);
		this.Wczytaj.addActionListener(this);
		this.Prawo.addActionListener(this);
		this.Gora.addActionListener(this);
		this.Okret1.addActionListener(this);
		this.Okret2.addActionListener(this);
		this.Okret3.addActionListener(this);
		this.Okret4.addActionListener(this);
		
		this.własne.setLayout(new GridLayout(10,10));
		własne.setPreferredSize(new Dimension(450, 350));	
		this.add(przeciwnik,BorderLayout.WEST);
		this.add(własne,BorderLayout.EAST);
		this.setSize(width, height);
		this.setTitle("Plansza");
		for(int j =0; j<10; j++) {  // Stworzenie obu plansz do gry
		for(int i=0; i<10; i++) {
			this.Pole[i][j] = new JButton(new ImageIcon("C:\\Users\\Dom\\eclipse-workspace\\Statki\\src\\X.png"));
			 Pole[i][j].setPreferredSize(new Dimension(10, 10));
			this.Pole[i][j].addActionListener(this);
			this.przeciwnik.add(this.Pole[i][j]);
			}
		}
		for(int j =0; j<10; j++) {
		for(int i=0; i<10; i++) {
			this.Pole2[i][j] = new JButton(new ImageIcon("C:\\Users\\Dom\\eclipse-workspace\\Statki\\src\\X.png"));
			 Pole2[i][j].setPreferredSize(new Dimension(10, 10));
			this.Pole2[i][j].addActionListener(this);
			this.własne.add(this.Pole2[i][j]);
			}
		}
		Centrum.setPreferredSize(new Dimension(200, 200));
		this.add(Centrum,BorderLayout.CENTER);
		setVisible(true);
	}
	public static void main(String[] args) {
		
		// Stworzenie planszy o danych wymiarach
		Plansza Plansza = new Plansza(1520,800);
		Plansza.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Plansza.setVisible(true);
	}
	@Override
	public void mouseDragged(MouseEvent e) {
	}
	@Override
	public void mouseMoved(MouseEvent e) {
	}
	@Override
	public void mouseClicked(MouseEvent e) {
	}
	@Override
	public void mousePressed(MouseEvent e) {
		}
	@Override
	public void mouseReleased(MouseEvent e) {
		}
	@Override
	public void mouseEntered(MouseEvent e) {
		}
	@Override
	public void mouseExited(MouseEvent e) {
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		
		// Mechanizm zapisu gry
		if (e.getSource() == Zapisz) {
		try {
			PrintWriter zapis = new PrintWriter("C:\\Users\\Dom\\eclipse-workspace\\Statki\\src\\zapis.txt");
			for(int j =0; j<10; j++) {
				for(int i=0; i<10; i++) {
					if(StrzałOddanyPrzeciwnik[i][j]==false && ObecnoscStatku2[i][j]==false) { zapis.println("0");}
					else if (StrzałOddanyPrzeciwnik[i][j]==true && ObecnoscStatku2[i][j]==false) { zapis.println("1");}
					else if (StrzałOddanyPrzeciwnik[i][j]==true && ObecnoscStatku2[i][j]==true) { zapis.println("2");}
					else if (StrzałOddanyPrzeciwnik[i][j]==false && ObecnoscStatku2[i][j]==true) { zapis.println("3");}
				}
			}
			zapis.close();
			
			
			PrintWriter zapisPrzeciwnik = new PrintWriter("C:\\Users\\Dom\\eclipse-workspace\\Statki\\src\\zapisPrzeciwnik.txt");
			for(int j =0; j<10; j++) {
				for(int i=0; i<10; i++) {
					if(StrzałOddany[i][j]==false && X.ObecnoscStatku[i][j]==false) { zapisPrzeciwnik.println("0");}
					else if (StrzałOddany[i][j]==true && X.ObecnoscStatku[i][j]==false) { zapisPrzeciwnik.println("1");}
					else if (StrzałOddany[i][j]==true && X.ObecnoscStatku[i][j]==true) { zapisPrzeciwnik.println("2");}
					else if (StrzałOddany[i][j]==false && X.ObecnoscStatku[i][j]==true) { zapisPrzeciwnik.println("3");}
				}
			}
			zapisPrzeciwnik.close();
			
		} catch (FileNotFoundException e1) {
			
			e1.printStackTrace();
		}
		}
		
		if (e.getSource() == Wczytaj) {
			// Mechanizm wczytania gry
			File file = new File("C:\\Users\\Dom\\eclipse-workspace\\Statki\\src\\zapis.txt");
			File file2 = new File("C:\\Users\\Dom\\eclipse-workspace\\Statki\\src\\zapisPrzeciwnik.txt");
			Scanner in;
			this.menu.remove(this.NowaGra);
			this.menu.remove(this.Wczytaj );
			
			this.menu.revalidate();
			this.menu.repaint();
			try {
				in = new Scanner(file);
				Statki=10;
				FazaGry=2;
						while(in.hasNextLine()) {
							for(int j =0; j<10; j++) {
								for(int i=0; i<10; i++) {
					String zdanie = in.nextLine();
					int num = Integer.parseInt(zdanie);
					if (num==0) {
						StrzałOddanyPrzeciwnik[i][j]=false; 
						ObecnoscStatku2[i][j]=false;
					}
					else if (num==1) {
						StrzałOddanyPrzeciwnik[i][j]=true; 
						ObecnoscStatku2[i][j]=false;
						Pole2[i][j].setIcon(new ImageIcon("C:\\Users\\Dom\\eclipse-workspace\\Statki\\src\\Woda.png"));
					}
					else if (num==2) {
						StrzałOddanyPrzeciwnik[i][j]=true; 
						ObecnoscStatku2[i][j]=true;
						Pole2[i][j].setIcon(new ImageIcon("C:\\Users\\Dom\\eclipse-workspace\\Statki\\src\\O.png"));
						Statki--;
					}
					else if (num==3) {
						StrzałOddanyPrzeciwnik[i][j]=false; 
						ObecnoscStatku2[i][j]=true;
						Pole2[i][j].setIcon(new ImageIcon("C:\\Users\\Dom\\eclipse-workspace\\Statki\\src\\OO.png"));
					}
					
						}
					}
				}
				in.close();
				
				
				Scanner in2;
				in2 = new Scanner(file2);
				X.StatkiPrzeciwnika=10;
				while(in2.hasNextLine()) {
					for(int j =0; j<10; j++) {
						for(int i=0; i<10; i++) {
			String zdanie2 = in2.nextLine();
			int num2 = Integer.parseInt(zdanie2);
			if (num2==0) {
				StrzałOddany[i][j]=false; 
				X.ObecnoscStatku[i][j]=false;
			}
			else if (num2==1) {
				StrzałOddany[i][j]=true; 
				X.ObecnoscStatku[i][j]=false;
				Pole[i][j].setIcon(new ImageIcon("C:\\Users\\Dom\\eclipse-workspace\\Statki\\src\\Woda.png"));
			}
			else if (num2==2) {
				StrzałOddany[i][j]=true; 
				X.ObecnoscStatku[i][j]=true;
				Pole[i][j].setIcon(new ImageIcon("C:\\Users\\Dom\\eclipse-workspace\\Statki\\src\\O.png"));
				X.StatkiPrzeciwnika--;
			}
			else if (num2==3) {
				StrzałOddanyPrzeciwnik[i][j]=false; 
				X.ObecnoscStatku[i][j]=true;
			}
			}
			}		
		}
				in2.close();
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
		} // Rozpoczęcie gry
		if(e.getSource() == NowaGra) {
			this.menu.remove(this.NowaGra);
			this.menu.remove(this.Wczytaj);
			this.menu.revalidate();
			this.menu.repaint();
			this.menu.add(this.Rozstaw);
			// Rozłożenie statków przeciwnika (losowe)
			while(X.StatkiPrzeciwnika<10) {
			check1 =X.getRandom(10);
			check2 =X.getRandom(10);
			if (X.StatkiPrzeciwnika<6) {
			NumerStatkuPrzeciwnik=X.getRandom(5);}
			if (X.StatkiPrzeciwnika>6) {
				NumerStatkuPrzeciwnik=X.getRandom(4);}
			if (X.StatkiPrzeciwnika>7) {
				NumerStatkuPrzeciwnik=X.getRandom(3);}
			if (X.StatkiPrzeciwnika>8) {
				NumerStatkuPrzeciwnik=1;}
			KierunekPrzeciwnik =X.getRandom(2);
			if (KierunekPrzeciwnik == 0) {
			X.Rozmieszczenie(check1, check2, Pole,NumerStatkuPrzeciwnik);
		}
			if (KierunekPrzeciwnik == 1) {
				X.Rozmieszczenie2(check1, check2, Pole,NumerStatkuPrzeciwnik);
			}
		}
			
		}
		
		if(e.getSource() == Rozstaw) {
		FazaGry	= 1;
		this.menu.remove(this.Rozstaw);
		this.menu.revalidate();
		this.menu.repaint();
		this.Centrum.add(this.Okret1);
		this.Centrum.add(this.Okret2);
		this.Centrum.add(this.Okret3);
		this.Centrum.add(this.Okret4);
		this.Centrum.add(this.Gora);
		this.Centrum.add(this.Prawo);
		}
		// Rozstaw statków
		if(e.getSource() == Okret1) { NumerStatku=1;}
		if(e.getSource() == Okret2) { NumerStatku=2;}
		if(e.getSource() == Okret3) { NumerStatku=3;}
		if(e.getSource() == Okret4) { NumerStatku=4;}
		if(e.getSource() == Prawo) { Kierunek=1;}
		if(e.getSource() == Gora) { Kierunek=2;}
		if (FazaGry==1) {
			if (Kierunek==1) {
			for (int b = 0; b < 10; b++) {
			for (int a = 0; a < 10; a++) {
		        if (e.getSource() == Pole2[a][b]) {
		       //check++;
		       //if (check>=5) {
		        	for(int c =0; c<NumerStatku;c++) {
		        			if(a>10-NumerStatku) {
		        				Pole2[a-c][b].setIcon(new ImageIcon("C:\\Users\\Dom\\eclipse-workspace\\Statki\\src\\OO.png"));
		        				if (ObecnoscStatku2[a-c][b]==false){
		        				Statki++;
		        				ObecnoscStatku2[a-c][b]=true;
		        				}
		        			}
		        			else{
		        				Pole2[a+c][b].setIcon(new ImageIcon("C:\\Users\\Dom\\eclipse-workspace\\Statki\\src\\OO.png"));
		        				if (ObecnoscStatku2[a+c][b]==false){
		        				Statki++;
		        				ObecnoscStatku2[a+c][b]=true;
		        				}
		        				}
		        	}
		        	if(NumerStatku==1) {Maks1++;}	
			        if(Maks1>1) {this.Centrum.remove(this.Okret1);this.Centrum.revalidate();
					this.Centrum.repaint(); NumerStatku=0;
			        }
		            if (Statki >6) { this.Centrum.remove(this.Okret4);this.Centrum.revalidate();
					this.Centrum.repaint(); NumerStatku=0;}
			        if (Statki >7) { this.Centrum.remove(this.Okret3);this.Centrum.revalidate();
					this.Centrum.repaint(); NumerStatku=0;}
			        if (Statki >8) { this.Centrum.remove(this.Okret2);this.Centrum.revalidate();
					this.Centrum.repaint(); NumerStatku=0;}
		        if (Statki >9) {
					FazaGry=2;
					this.Centrum.remove(this.Okret1);
					this.Centrum.remove(this.Okret2);
					this.Centrum.remove(this.Okret3);
					this.Centrum.remove(this.Okret4);
					this.Centrum.remove(this.Prawo);
					this.Centrum.remove(this.Gora);
					this.Centrum.revalidate();
					this.Centrum.repaint();
				}
		        }
		        }
			}
		}
			if (Kierunek==2) {
				for (int b = 0; b < 10; b++) {
				for (int a = 0; a < 10; a++) {
			        if (e.getSource() == Pole2[a][b]) {
			       //check++;
			       //if (check>=5) {
			        	for(int c =0; c<NumerStatku;c++) {
			        			if(b>NumerStatku-2) {
			        				Pole2[a][b-c].setIcon(new ImageIcon("C:\\Users\\Dom\\eclipse-workspace\\Statki\\src\\OO.png"));
			        				if (ObecnoscStatku2[a][b-c]==false){
			        				Statki++;
			        				ObecnoscStatku2[a][b-c]=true;
			        				
			        				}
			        			}
			        			else{
			        				Pole2[a][b+c].setIcon(new ImageIcon("C:\\Users\\Dom\\eclipse-workspace\\Statki\\src\\OO.png"));
			        				if (ObecnoscStatku2[a][b+c]==false){
			        				Statki++;
			        				ObecnoscStatku2[a][b+c]=true;
			        				}
			        				}
			        	}
			        if(NumerStatku==1) {Maks1++;}	
			        if(Maks1>1) {this.Centrum.remove(this.Okret1);this.Centrum.revalidate();
					this.Centrum.repaint(); NumerStatku=0;
			        }
			        if (Statki >6) { this.Centrum.remove(this.Okret4);this.Centrum.revalidate();
					this.Centrum.repaint();NumerStatku=0;}
			        if (Statki >7) { this.Centrum.remove(this.Okret3);this.Centrum.revalidate();
					this.Centrum.repaint();NumerStatku=0;}
			        if (Statki >8) { this.Centrum.remove(this.Okret2);this.Centrum.revalidate();
					this.Centrum.repaint();NumerStatku=1;}
			        
			        
			        if (Statki >9) {
						FazaGry=2;
						
						
						this.Centrum.remove(this.Okret1);
						this.Centrum.remove(this.Okret2);
						this.Centrum.remove(this.Okret3);
						this.Centrum.remove(this.Okret4);
						this.Centrum.remove(this.Prawo);
						this.Centrum.remove(this.Gora);
						this.Centrum.revalidate();
						this.Centrum.repaint();
					}
			        }
			        }
				}
			}
		} // Rozpoczęcie gry (jeśli FazaGry równa się 2)
		if (FazaGry==2) {
			this.menu.add(this.Zapisz);
			this.menu.revalidate();
			this.menu.repaint();
		for (int b = 0; b < 10; b++) {
		for (int a = 0; a < 10; a++) {
	        if (e.getSource() == Pole[a][b]) {
	        
	        	if (StrzałOddany[a][b]==false){ // Mechanizm strzelania komputera, zmienna StrzałTrafiony określa sytuację na którą reaguje
	        		
	        		if (StrzałTrafiony==0) {
    				check3 =X.getRandom(10);
    				check4 =X.getRandom(10);
    				while(StrzałOddanyPrzeciwnik[check3][check4]==true) {check3 =X.getRandom(10); check4 =X.getRandom(10);}
    				StrzałOddanyPrzeciwnik[check3][check4]=true;
    				if(ObecnoscStatku2[check3][check4]==true) {
    				Statki--;
    				if(check3+1<10) {
    				if(StrzałOddanyPrzeciwnik[check3+1][check4]==false) {
    				StrzałTrafiony=1;
    				}
    				else {
    					if(check3-1>=0) {
    					if(StrzałOddanyPrzeciwnik[check3-1][check4]==false) {
    					StrzałTrafiony=2;
    					}
    					else { StrzałTrafiony=0;}
    					}
    					else {
    					StrzałTrafiony=0;
    					}
    					}
    				}
    				else if(check3-1>=0) {
    				if(StrzałOddanyPrzeciwnik[check3-1][check4]==false) {
    	    			StrzałTrafiony=2;
    	    		}
    				}
    				else if(check3-1<0) {StrzałTrafiony=0;}
    				
    				Pole2[check3][check4].setIcon(new ImageIcon("C:\\Users\\Dom\\eclipse-workspace\\Statki\\src\\O.png"));
    				}
    				else { 
    				StrzałTrafiony=0;
    				Pole2[check3][check4].setIcon(new ImageIcon("C:\\Users\\Dom\\eclipse-workspace\\Statki\\src\\Woda.png"));
    				}
    				}
	        		
	        		else if (StrzałTrafiony==1) {
	        		
	        			StrzałOddanyPrzeciwnik[check3+1][check4]=true;
	        			if(ObecnoscStatku2[check3+1][check4]==true) {
	        				Statki--; 
	        				Pole2[check3+1][check4].setIcon(new ImageIcon("C:\\Users\\Dom\\eclipse-workspace\\Statki\\src\\O.png"));
	        				if(check3+2<10) {
	        				if(StrzałOddanyPrzeciwnik[check3+2][check4]==false) {
	            				StrzałTrafiony=3;
	            				}
	        				else if(check3-1>=0) {
	        				if(StrzałOddanyPrzeciwnik[check3-1][check4]==false) {
		        				StrzałTrafiony=2;}
	        				}
	        				else {StrzałTrafiony=0;}
	        				}
	        			
		        				else {
		        					if(StrzałOddanyPrzeciwnik[check3-1][check4]==false) {
				        				StrzałTrafiony=2;}
		        					else if (check4+1<10){
		        					if (StrzałOddanyPrzeciwnik[check3][check4+1]==false) {
		        					StrzałTrafiony=10;	
		        					} else {
		        						if (check4-1>=0) {
		        						if (StrzałOddanyPrzeciwnik[check3][check4-1]==false) {
	        							StrzałTrafiony=11;
		        						} else {StrzałTrafiony=0;}
	        						} else {StrzałTrafiony=0;}}
		        					} else {
		        						if (StrzałOddanyPrzeciwnik[check3][check4-1]==false) {
		        							StrzałTrafiony=11;
		        						} else {StrzałTrafiony=0;}
		        						
		        						
		        					}
		        					
	        			}
	        				}
	        			if(ObecnoscStatku2[check3+1][check4]==false) {
	        				if(check3-1>=0) {
	        				if(StrzałOddanyPrzeciwnik[check3-1][check4]==false) {
		        				StrzałTrafiony=2;
		        				} else {
		        					if(check4+1<10) {
		        					if(StrzałOddanyPrzeciwnik[check3][check4+1]==false) {
		        						StrzałTrafiony=10;
		        					} else {
		        						if(check4-1>=0) {
		        						if(StrzałOddanyPrzeciwnik[check3][check4-1]==false) {
		        						StrzałTrafiony=11;
		        						} else {StrzałTrafiony=0;}
		        						} else {StrzałTrafiony=0;}
		        						}
		        					} else {
		        						if(StrzałOddanyPrzeciwnik[check3][check4-1]==false) {StrzałTrafiony=11;}
		        						else {StrzałTrafiony=0;}
		        						}
		        					}
	        				}
		        				else { 
		        					if(check4+1<10) {
		        					if(StrzałOddanyPrzeciwnik[check3][check4+1]==false) { StrzałTrafiony=10;	
		        					}
		        					else {StrzałTrafiony=0;}
		        					}
		        					else{StrzałTrafiony=0;}
		        					
		        				}
	        				Pole2[check3+1][check4].setIcon(new ImageIcon("C:\\Users\\Dom\\eclipse-workspace\\Statki\\src\\Woda.png"));
	        				}
	        		
	        		}
	        		else if (StrzałTrafiony==3) {
		        		
	        			StrzałOddanyPrzeciwnik[check3+2][check4]=true;
	        			if(ObecnoscStatku2[check3+2][check4]==true) {
	        				Statki--; 
	        				Pole2[check3+2][check4].setIcon(new ImageIcon("C:\\Users\\Dom\\eclipse-workspace\\Statki\\src\\O.png"));
	        				if(check3+3<10) {
	        				if(StrzałOddanyPrzeciwnik[check3+3][check4]==false) {
	        				StrzałTrafiony=5;
	        				}
	        				else {StrzałTrafiony=0; }
	        				}
	        				else if(StrzałOddanyPrzeciwnik[check3-1][check4]==false) {
	        				StrzałTrafiony=2;}
	        				else {StrzałTrafiony=0;}
	        				}
	        			if(ObecnoscStatku2[check3+2][check4]==false) {
	        				if(check3-1>=0) {
	        				if(StrzałOddanyPrzeciwnik[check3-1][check4]==false) {
	        				StrzałTrafiony=2;
	        				} else {StrzałTrafiony=0;}
	        				}
	        				else { StrzałTrafiony=0;}
	        				Pole2[check3+2][check4].setIcon(new ImageIcon("C:\\Users\\Dom\\eclipse-workspace\\Statki\\src\\Woda.png"));
	        				}
	        		}
	        		else  if(StrzałTrafiony==5) {
	        				StrzałOddanyPrzeciwnik[check3+3][check4]=true;
		        			if(ObecnoscStatku2[check3+3][check4]==true) {
		        				Statki--; 
		        				Pole2[check3+3][check4].setIcon(new ImageIcon("C:\\Users\\Dom\\eclipse-workspace\\Statki\\src\\O.png"));
		        			 StrzałTrafiony=0;
		        				}
		        			if(ObecnoscStatku2[check3+3][check4]==false) {
		        				if(check3-1>=0) {
		        				if(StrzałOddanyPrzeciwnik[check3-1][check4]==false) {
			        				StrzałTrafiony=2;
		        					}
		        				else {StrzałTrafiony=0;}
			        				}
			        				else { StrzałTrafiony=0;}
		        				Pole2[check3+3][check4].setIcon(new ImageIcon("C:\\Users\\Dom\\eclipse-workspace\\Statki\\src\\Woda.png"));
		        				}	
	        				
	        			}
	        		
	        		
	        		
	        		else if (StrzałTrafiony==2) {
		        		
		        			StrzałOddanyPrzeciwnik[check3-1][check4]=true;
		        			if(ObecnoscStatku2[check3-1][check4]==true) {
		        				Statki--; 
		        				Pole2[check3-1][check4].setIcon(new ImageIcon("C:\\Users\\Dom\\eclipse-workspace\\Statki\\src\\O.png"));
		        				if(check3-2>=0) {
		        				if(StrzałOddanyPrzeciwnik[check3-2][check4]==false) {
		            				StrzałTrafiony=4;
		            				}
		        				else {StrzałTrafiony=0;}
		        				}
		        				else { 
		        					StrzałTrafiony=0;
		        					}
		        				}
		        			if(ObecnoscStatku2[check3-1][check4]==false) {
		        				if(check4+1<10) {
		        				if(StrzałOddanyPrzeciwnik[check3][check4+1]==false) {
		        				StrzałTrafiony=10;
		        				}
		        				else if(check4-1>=0) {
		        				if(StrzałOddanyPrzeciwnik[check3][check4-1]==false) {
		        				StrzałTrafiony=11;
		        				}
		        				else {StrzałTrafiony=0;}
		        				}
		        				}
		        				else if(check4-1>=0) {
		        				if(StrzałOddanyPrzeciwnik[check3][check4-1]==false) {
		        				StrzałTrafiony=11;
		        				}
		        				}
		        				else {StrzałTrafiony=0;}
		        				
		        				Pole2[check3-1][check4].setIcon(new ImageIcon("C:\\Users\\Dom\\eclipse-workspace\\Statki\\src\\Woda.png"));
		        				}
	        		
	        		
		        		}
	        		else if (StrzałTrafiony==4) {
	        			StrzałOddanyPrzeciwnik[check3-2][check4]=true;
	        			if(ObecnoscStatku2[check3-2][check4]==true) {
	        				Statki--; 
	        				Pole2[check3-2][check4].setIcon(new ImageIcon("C:\\Users\\Dom\\eclipse-workspace\\Statki\\src\\O.png"));
	        				if(check3-3>=0) {
	        				if(StrzałOddanyPrzeciwnik[check3-3][check4]==false) {
	        				StrzałTrafiony=6;
	        				}
	        				else {StrzałTrafiony=0;}
	        				}
	        				else { StrzałTrafiony=0; }
	        				}
	        			if(ObecnoscStatku2[check3-2][check4]==false) {
	        				StrzałTrafiony=0;
	        				Pole2[check3-2][check4].setIcon(new ImageIcon("C:\\Users\\Dom\\eclipse-workspace\\Statki\\src\\Woda.png"));
	        				}
	        		}
	        		
	        		else if (StrzałTrafiony==6) {
			        		
		        			StrzałOddanyPrzeciwnik[check3-3][check4]=true;
		        			if(ObecnoscStatku2[check3-3][check4]==true) {
		        				Statki--; 
		        				Pole2[check3-3][check4].setIcon(new ImageIcon("C:\\Users\\Dom\\eclipse-workspace\\Statki\\src\\O.png"));
		        				StrzałTrafiony=0;
		        				}
		        			if(ObecnoscStatku2[check3-3][check4]==false) {
		        				StrzałTrafiony=0;
		        				Pole2[check3-3][check4].setIcon(new ImageIcon("C:\\Users\\Dom\\eclipse-workspace\\Statki\\src\\Woda.png"));
		        				}
	        			}
	        		
	        		else if (StrzałTrafiony==10) {
	        		
	        			StrzałOddanyPrzeciwnik[check3][check4+1]=true;
	        			if(ObecnoscStatku2[check3][check4+1]==true) {
	        			Statki--;
	        			Pole2[check3][check4+1].setIcon(new ImageIcon("C:\\Users\\Dom\\eclipse-workspace\\Statki\\src\\O.png"));
	        			if(check4+2<10){
	        			if(StrzałOddanyPrzeciwnik[check3][check4+2]==false) {
	        			StrzałTrafiony=12;	
	        			}
	        			else {
	        				if(StrzałOddanyPrzeciwnik[check3][check4-1]==false) {
	        				StrzałTrafiony=11;}
	        				else {StrzałTrafiony=0;}
	        			}
	        			}
	        			else {StrzałTrafiony=0;}
	        			}
	        			if(ObecnoscStatku2[check3][check4+1]==false) {
	        				if(check4-1>=0) {
	        				if(StrzałOddanyPrzeciwnik[check3][check4-1]==false) {
	        				StrzałTrafiony=11;
	        				}
	        				else {StrzałTrafiony=0;}
	        				}
	        				else {StrzałTrafiony=0;}
	        				
	        				Pole2[check3][check4+1].setIcon(new ImageIcon("C:\\Users\\Dom\\eclipse-workspace\\Statki\\src\\Woda.png"));
	        			}
	        		}
	        		else if (StrzałTrafiony==11) {
		        		
	        			StrzałOddanyPrzeciwnik[check3][check4-1]=true;
	        			if(ObecnoscStatku2[check3][check4-1]==true) {
	        			Statki--;
	        			Pole2[check3][check4-1].setIcon(new ImageIcon("C:\\Users\\Dom\\eclipse-workspace\\Statki\\src\\O.png"));
	        			if(check4-2>=0){
	        			if(StrzałOddanyPrzeciwnik[check3][check4-2]==false) {
	        			StrzałTrafiony=13;	
	        			}
	        			else {StrzałTrafiony=0;}
	        			}else {StrzałTrafiony=0;}
	        			}
	        			if(ObecnoscStatku2[check3][check4-1]==false) {
	        				StrzałTrafiony=0;
	        				Pole2[check3][check4-1].setIcon(new ImageIcon("C:\\Users\\Dom\\eclipse-workspace\\Statki\\src\\Woda.png"));
	        			}
	        		}
	        		else if (StrzałTrafiony==13) {
		        		
	        			StrzałOddanyPrzeciwnik[check3][check4-2]=true;
	        			if(ObecnoscStatku2[check3][check4-2]==true) {
	        			Statki--;
	        			Pole2[check3][check4-2].setIcon(new ImageIcon("C:\\Users\\Dom\\eclipse-workspace\\Statki\\src\\O.png"));
	        			if(check4-3>=0){
	        			if(StrzałOddanyPrzeciwnik[check3][check4-2]==false) {
	        			StrzałTrafiony=15;	
	        			}
	        			
	        			else {StrzałTrafiony=0;}
	        			}
	        			else {StrzałTrafiony=0;}
	        			}
	        			if(ObecnoscStatku2[check3][check4-2]==false) {
	        				StrzałTrafiony=0;
	        				Pole2[check3][check4-2].setIcon(new ImageIcon("C:\\Users\\Dom\\eclipse-workspace\\Statki\\src\\Woda.png"));
	        			}
	        		}
	        		else if (StrzałTrafiony==12) {
		        		
	        			StrzałOddanyPrzeciwnik[check3][check4+2]=true;
	        			if(ObecnoscStatku2[check3][check4+2]==true) {
	        			Statki--;
	        			Pole2[check3][check4+2].setIcon(new ImageIcon("C:\\Users\\Dom\\eclipse-workspace\\Statki\\src\\O.png"));
	        			if(check4+3<10){
	        			if(StrzałOddanyPrzeciwnik[check3][check4+3]==false) {
	        			StrzałTrafiony=14;	
	        			}
	        			
	        			else {StrzałTrafiony=0;}
	        			}
	        			}
	        			if(ObecnoscStatku2[check3][check4+2]==false) {
	        				if(check4-1>=0) {
	        				if(StrzałOddanyPrzeciwnik[check3][check4-1]==false) {
	        				StrzałTrafiony=11;
	        				} else {StrzałTrafiony=0;}
	        				} else {StrzałTrafiony=0;}
	        				
	        				Pole2[check3][check4+2].setIcon(new ImageIcon("C:\\Users\\Dom\\eclipse-workspace\\Statki\\src\\Woda.png"));
	        			}
	        		}
	        		else if (StrzałTrafiony==14) {
		        		
	        			StrzałOddanyPrzeciwnik[check3][check4+3]=true;
	        			if(ObecnoscStatku2[check3][check4+3]==true) {
	        			Statki--;
	        			Pole2[check3][check4+3].setIcon(new ImageIcon("C:\\Users\\Dom\\eclipse-workspace\\Statki\\src\\O.png"));
	        		StrzałTrafiony=0;
	        			}
	        			if(ObecnoscStatku2[check3][check4+3]==false) {
	        				StrzałTrafiony=0;
	        				Pole2[check3][check4+3].setIcon(new ImageIcon("C:\\Users\\Dom\\eclipse-workspace\\Statki\\src\\Woda.png"));
	        			}
	        		}
	        		else if (StrzałTrafiony==15) {
		        		
	        			StrzałOddanyPrzeciwnik[check3][check4-3]=true;
	        			if(ObecnoscStatku2[check3][check4-3]==true) {
	        			Statki--;
	        			Pole2[check3][check4-3].setIcon(new ImageIcon("C:\\Users\\Dom\\eclipse-workspace\\Statki\\src\\O.png"));
	        		StrzałTrafiony=0;
	        			}
	        			if(ObecnoscStatku2[check3][check4-3]==false) {
	        				StrzałTrafiony=0;
	        				Pole2[check3][check4-3].setIcon(new ImageIcon("C:\\Users\\Dom\\eclipse-workspace\\Statki\\src\\Woda.png"));
	        			}
	        		}
	        		
	        		
	        		
	        		
	        		
	        		
	        		
	        	
	        if (X.ObecnoscStatku[a][b]==true) {
	        Pole[a][b].setIcon(new ImageIcon("C:\\Users\\Dom\\eclipse-workspace\\Statki\\src\\O.png"));
	        X.StatkiPrzeciwnika--;
	        if (X.StatkiPrzeciwnika==0) {this.Centrum.add(this.Zwycięstwo);FazaGry=0;this.menu.remove(this.Zapisz); }
	        }
	        else {Pole[a][b].setIcon(new ImageIcon("C:\\Users\\Dom\\eclipse-workspace\\Statki\\src\\Woda.png"));};
	        StrzałOddany[a][b]=true;
	        }
	        	else {} //Ważny else, jeśli gracz strzela w to samo miejsce, program nic nie zrobi
	       
	        }
	        
	       }
		}
		if (Statki==0) {this.Centrum.add(this.Porażka); FazaGry=0; this.menu.remove(this.Zapisz); }
		}
		
	}
	
	//}
	
	@Override
	public void stateChanged(ChangeEvent e) {
		// TODO Auto-generated method stub
	}
}
