
ACTION ActionPLCOpen: 

(* Action is used to determine the State of every axis according to PLC Open *)
	
	CASE gAxisXState OF
		mcAXIS_DISABLED:
			AxisXInfo := 'The power stage is switched off';
		mcAXIS_STANDSTILL:
			AxisXInfo := 'The power stage switched on; the axis is NOT moving';
		mcAXIS_HOMING:
			AxisXInfo := 'Axis is executing a homing procedure';
		mcAXIS_STOPPING:
			AxisXInfo := 'Axis currently stopping OR "Execute" is still set FOR "MC_Stop"' ;	
		mcAXIS_DISCRETE_MOTION:
			AxisXInfo := 'Movement with defined end position active';
		mcAXIS_CONTINUOUS_MOTION:
			AxisXInfo := 'Movement without defined target position is active';
		mcAXIS_SYNCHRONIZED_MOTION:
			AxisXInfo := 'Axis is coupled TO another axis (master-slave coupling)';
		mcAXIS_ERRORSTOP:
			AxisXInfo := 'Axis stopped due TO drive error';
		mcAXIS_STARTUP:
			AxisXInfo := 'The axis is in the startup phase.';
		mcAXIS_INVALID_CONFIGURATION:
			AxisXInfo := 'An error occurred in the configuration OF the axis.';
	END_CASE;
	
	CASE gAxisYState OF
		mcAXIS_DISABLED:
			AxisYInfo := 'The power stage is switched off';
		mcAXIS_STANDSTILL:
			AxisYInfo := 'The power stage switched on; the axis is NOT moving';
		mcAXIS_HOMING:
			AxisYInfo := 'Axis is executing a homing procedure';
		mcAXIS_STOPPING:
			AxisYInfo := 'Axis currently stopping OR "Execute" is still set FOR "MC_Stop"' ;	
		mcAXIS_DISCRETE_MOTION:
			AxisYInfo := 'Movement with defined end position active';
		mcAXIS_CONTINUOUS_MOTION:
			AxisYInfo := 'Movement without defined target position is active';
		mcAXIS_SYNCHRONIZED_MOTION:
			AxisYInfo := 'Axis is coupled TO another axis (master-slave coupling)';
		mcAXIS_ERRORSTOP:
			AxisYInfo := 'Axis stopped due TO drive error';
		mcAXIS_STARTUP:
			AxisYInfo := 'The axis is in the startup phase.';
		mcAXIS_INVALID_CONFIGURATION:
			AxisYInfo := 'An error occurred in the configuration OF the axis.';
	END_CASE;
	
	CASE gAxisZState OF
		mcAXIS_DISABLED:
			AxisZInfo := 'The power stage is switched off';
		mcAXIS_STANDSTILL:
			AxisZInfo := 'The power stage switched on; the axis is NOT moving';
		mcAXIS_HOMING:
			AxisZInfo := 'Axis is executing a homing procedure';
		mcAXIS_STOPPING:
			AxisZInfo := 'Axis currently stopping OR "Execute" is still set FOR "MC_Stop"' ;	
		mcAXIS_DISCRETE_MOTION:
			AxisZInfo := 'Movement with defined end position active';
		mcAXIS_CONTINUOUS_MOTION:
			AxisZInfo := 'Movement without defined target position is active';
		mcAXIS_SYNCHRONIZED_MOTION:
			AxisZInfo := 'Axis is coupled TO another axis (master-slave coupling)';
		mcAXIS_ERRORSTOP:
			AxisZInfo := 'Axis stopped due TO drive error';
		mcAXIS_STARTUP:
			AxisZInfo := 'The axis is in the startup phase.';
		mcAXIS_INVALID_CONFIGURATION:
			AxisZInfo := 'An error occurred in the configuration OF the axis.';
	END_CASE;
	
	CASE gSpindleState OF
		mcAXIS_DISABLED:
			SpindleInfo := 'The power stage is switched off';
		mcAXIS_STANDSTILL:
			SpindleInfo := 'The power stage switched on; the axis is NOT moving';
		mcAXIS_HOMING:
			SpindleInfo := 'Axis is executing a homing procedure';
		mcAXIS_STOPPING:
			SpindleInfo := 'Axis currently stopping OR "Execute" is still set FOR "MC_Stop"' ;	
		mcAXIS_DISCRETE_MOTION:
			SpindleInfo := 'Movement with defined end position active';
		mcAXIS_CONTINUOUS_MOTION:
			SpindleInfo := 'Movement without defined target position is active';
		mcAXIS_SYNCHRONIZED_MOTION:
			SpindleInfo := 'Axis is coupled TO another axis (master-slave coupling)';
		mcAXIS_ERRORSTOP:
			SpindleInfo := 'Axis stopped due TO drive error';
		mcAXIS_STARTUP:
			SpindleInfo := 'The axis is in the startup phase.';
		mcAXIS_INVALID_CONFIGURATION:
			SpindleInfo := 'An error occurred in the configuration OF the axis.';
	END_CASE;
	
END_ACTION
