(*********************************************************************************
 * Copyright:   B&R Automation
 * Author:      Bartosz Bobkowski 
 * Created:     July 29, 2022/12:50 PM 
 * Task:        Program checks PLCOpen state of the machine and manages the chart
 *********************************************************************************)


PROGRAM _CYCLIC
	
	ActionPLCOpen;	
	ActionCharts;
	
	
END_PROGRAM




