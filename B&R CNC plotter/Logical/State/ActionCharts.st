
ACTION ActionCharts: 

	(* Action is used to show the chart chosen by the user *)
	CurrentChart := ' ';
	
	IF ChartX THEN
		CurrentChart := 'Position X';
	END_IF;
	
	IF ChartY THEN
		CurrentChart := 'Position Y';
	END_IF;
	
	IF ChartZ THEN
		CurrentChart := 'Position Z';
	END_IF;
	
		
	IF ChartX2 THEN
		CurrentChart := 'Position X (close-up)';
	END_IF;
	
	IF ChartVelX THEN
		CurrentChart := 'Velocity X';
	END_IF;
	
	IF ChartVelY THEN
		CurrentChart := 'Velocity Y';
	END_IF;
	
	IF ChartVelZ THEN
		CurrentChart := 'Velocity Z';
	END_IF;
	
	IF ChartVelSpindle THEN
		CurrentChart := 'Velocity Spindle';
	END_IF;
	
END_ACTION
