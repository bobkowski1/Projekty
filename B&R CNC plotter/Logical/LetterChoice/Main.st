(*********************************************************************************
 * Copyright:  B&R Automation 
 * Author:     Bartosz Bobkowski
 * Created:    July 29, 2022/12:51 PM 
 * Task:       Program reads input from user and sets the programs to be executed
 *********************************************************************************)



PROGRAM _INIT
	(* Insert code here *)
	 
END_PROGRAM

PROGRAM _CYCLIC
	
	IF gLetterChoice  = LETTER_X THEN
		gLetterLength := 75;
		gPrintLetter := 'Letter_X';
	END_IF;
	
	IF gLetterChoice = LETTER_I THEN
		gLetterLength := 10;
		gPrintLetter := 'Letter_I';
	END_IF;
	
	IF gLetterChoice = LETTER_I2 THEN
		gLetterLength := 10;
		gPrintLetter := 'Letter_I2';
	END_IF;
	
	IF gLetterChoice = LETTER_R THEN
		gLetterLength := 50;
		gPrintLetter := 'Letter_R';
	END_IF;
	
	IF gLetterChoice = LETTER_W THEN
		gLetterLength := 100;
		gPrintLetter := 'Letter_W';
	END_IF;
	
	IF gLetterChoice = LETTER_U THEN
		gLetterLength := 75;
		gPrintLetter := 'Letter_U';
	END_IF;
	
	
	(*Advanced Work mode and Engraving Words modes are chosen. Program now will select programs to execute *)
	IF gMode = 'ADVANCED' AND gModeAdvanced = 'WORD ENGRAVING' THEN
		FOR i:=0 TO 10 DO
			gWord[i] := 	MID(gWordInput,1,i);
		END_FOR;
		
		FOR i:=0 TO 10 DO
			IF gWord[i] = 'R' THEN
				gWordProgram[i] := 'Letter_R';
				gCurrentLength := 	gCurrentLength + 60;
				gCurrentLetterLength[i] := 60;
			ELSIF gWord[i] = 'U' THEN
				gWordProgram[i] := 'Letter_U';
				gCurrentLength := 	gCurrentLength + 85;
				gCurrentLetterLength[i] := 85;
			ELSIF gWord[i] = 'I' THEN
				gWordProgram[i] := 'Letter_I';
				gCurrentLength := 	gCurrentLength + 20;
				gCurrentLetterLength[i] := 20;
			ELSIF gWord[i] = 'i' THEN
				gWordProgram[i] := 'Letter_I2';
				gCurrentLength := 	gCurrentLength + 20;
				gCurrentLetterLength[i] := 20;
			ELSIF gWord[i] = 'X' THEN
				gWordProgram[i] := 'Letter_X';
				gCurrentLength := 	gCurrentLength + 85;
				gCurrentLetterLength[i] := 85;
			ELSIF gWord[i] = 'W' THEN
				gWordProgram[i] := 'Letter_W';
				gCurrentLength := 	gCurrentLength + 110;
				gCurrentLetterLength[i] := 110;
			END_IF;
			
			IF i = 10 THEN
				gWordLength := gCurrentLength;
				gCurrentLength := 0;
			END_IF;
		END_FOR;
		
		
		
	END_IF;
	
	
	
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

