(*********************************************************************************
 * Copyright: B&R Automation 
 * Author:    Bartosz Bobkowski
 * Created:   July 29, 2022/12:46 PM
 * Task:      Functionality of Part Sensor is handled here 
 *********************************************************************************)


PROGRAM _INIT
	(* Insert code here *)

	
END_PROGRAM

PROGRAM _CYCLIC

	(* Program measure the length of part using rising and falling edges of sensor ouput *)
	IF gMeasureON = TRUE THEN
		R_TRIGG.CLK := gSensor;
		R_TRIGG();
	
		F_TRIGG.CLK := gSensor;
		F_TRIGG();
	
		IF R_TRIGG.Q = TRUE THEN	
			lengthStart := gMotorParametersX.Position;
		END_IF;
	
		IF F_TRIGG.Q = TRUE AND lengthStart > 0 THEN	
			lengthStop := gMotorParametersX.Position;
			gPartLength := lengthStop - lengthStart; 
		END_IF;
	END_IF;	
	
	IF gMeasureON = FALSE THEN
		lengthStart := 0;
		lengthStop := 0;
	END_IF;
	
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

