
ACTION ActionAllMotorsOff: 
	MpAxisBasic_0[1].MoveAbsolute 		:= FALSE;
	MpAxisBasic_0[2].MoveAbsolute 		:= FALSE;
	MpAxisBasic_0[0].MoveAdditive 		:= FALSE;
	MpAxisBasic_0[0].MoveVelocity 		:= FALSE;
	MpAxisBasic_Spindle.MoveVelocity 	:= FALSE;
END_ACTION
