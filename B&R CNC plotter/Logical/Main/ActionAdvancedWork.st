
ACTION ActionAdvancedWork: 

	gModeWork := 'ADVANCED';
	
	CASE AdvancedMode OF	
	
		(*Program is waiting to choose Cyclic Work or Word Engraving *)
		ADVANCED_WAITING:
			ActionAllMotorsOff;
			gModeAdvanced := 'WAITING';
			gPartLength := 0;
			
			IF CyclicWorkConfirm AND CyclicWork THEN
				AdvancedMode := CYCLICAL_WORK;
				AllCyclesFinished := FALSE;
			END_IF;
			
			IF WordEngravingConfirm AND WordEngraving THEN
				AdvancedMode := WORD_ENGRAVING;
				WordFinished := FALSE;
			END_IF;
		
			
			(* Cyclical mode is chosen by user *)
		CYCLICAL_WORK:
			gModeAdvanced := 'CYCLICAL WORK';
			IF NumberOfCycles <> 0 THEN
				WorkMode := WORK_MEASURING;
				IF CyclesFinished = NumberOfCycles THEN
					AdvancedMode := ADVANCED_WAITING;
					WorkMode := WORK_ADVANCED;
					CyclesFinished := 0;
					AllCyclesFinished := TRUE;
				END_IF;	
			END_IF;		
		
			
			(* Engraving mode is chosen by user *)
		WORD_ENGRAVING:
			gModeAdvanced := 'WORD ENGRAVING';
			IF gWordLength > 0 AND NOT WordFinished THEN
				AdvancedMode := ADVANCED_MEASURING;
				iA := 1;
			END_IF;
			
			(* Measuring of the part to be engraved. Only in Word Engraving mode *)
		ADVANCED_MEASURING:	
			gModeAdvanced := 'MEASURING';
			MpAxisBasic_0[0].Home := FALSE;
			MpAxisBasic_0[0].MoveVelocity := TRUE;
			IF gPartLength > (gWordLength + 40) THEN
				AdvancedMode:= ADVANCED_POSITIONING;
				MpAxisBasic_0[0].MoveVelocity := FALSE;
			ELSIF gPartLength > 0 THEN
				ErrorMode := ERROR_PART_SHORT;
			END_IF;		
			
			(*Positioning of part. Only in Word Engraving mode *)
		ADVANCED_POSITIONING:
			gModeAdvanced := 'POSITIONING';
			AxisParameters[0].Distance := gPartLength*(-1) + 20;
			MpAxisBasic_0[0].MoveAdditive := TRUE;
				
			IF MpAxisBasic_0[0].MoveDone THEN
				IF gSensor THEN
					AxisParameters[1].Position := 200;
					AxisParameters[2].Position := 110;
					MpAxisBasic_0[1].MoveAbsolute := TRUE;
					MpAxisBasic_0[2].MoveAbsolute := TRUE;
				ELSE 
					ErrorMode:=ERROR_SENSOR;
				END_IF;
			END_IF; 
				
			IF MpAxisBasic_0[0].MoveDone AND MpAxisBasic_0[1].MoveDone AND MpAxisBasic_0[2].MoveDone   THEN
				ActionAllMotorsOff;
				//MpAxisBasic_0[0].Home := TRUE;
				AdvancedMode:= ADVANCED_CNC;
			END_IF;
			
			
			(* CNC Program is executed. Only in Word Engraving Mode *)
		ADVANCED_CNC:
			gModeAdvanced := 'CNC PROGRAM';
			IF  MpAxisBasic_0[0].Info.PLCopenState <> mcAXIS_HOMING  THEN
				MpAxisBasic_0[0].Home := FALSE;
				CncParameters.ProgramName := gWordProgram[iA];
				gCurrentProgram := gWordProgram[iA];
				MpCnc3Axis_0.MoveProgram := TRUE;
				IF gCurrentProgram = '' THEN
					MpCnc3Axis_0.MoveProgram := FALSE;
					AdvancedMode:= ADVANCED_FINISHED;
				END_IF;
			END_IF;
			IF MpCnc3Axis_0.MoveDone THEN
				AdvancedMode:= ADVANCED_FINISHED;
				MpCnc3Axis_0.MoveProgram := FALSE;
				ActionAllMotorsOff;
				//MpAxisBasic_0[0].Home := TRUE;
			END_IF;	
			
			(* CNC Program is finished, Another lether will be engraved. Only in Word Engraving Mode *)
		ADVANCED_FINISHED:
			gModeAdvanced := 'LETTER FINISHED';
			IF iA < 10 THEN
				AxisParameters[0].Distance := ( gCurrentLetterLength[iA+1]/2);
			END_IF;
			
			IF MpAxisBasic_0[0].Info.PLCopenState <> mcAXIS_HOMING THEN
				MpAxisBasic_0[0].MoveAdditive := TRUE;
				AxisParameters[1].Position := 200;
				AxisParameters[2].Position := 110;
				MpAxisBasic_0[1].MoveAbsolute := TRUE;
				MpAxisBasic_0[2].MoveAbsolute := TRUE;
			END_IF;	
			IF MpAxisBasic_0[0].MoveDone AND MpAxisBasic_0[1].MoveDone AND MpAxisBasic_0[2].MoveDone   THEN
				ActionAllMotorsOff;
				MpAxisBasic_0[0].Home := FALSE;
				AdvancedMode:= ADVANCED_CNC;
				iA := iA + 1;
			END_IF;
			
			IF iA = 10 THEN
				WordFinished := TRUE;
				AdvancedMode := ADVANCED_WAITING;
			END_IF;
	END_CASE;
	
	
END_ACTION
