(*********************************************************************************
 * Copyright: B&R Automation   
 * Author:    Bartosz Bobkowski 
 * Created:   July 29, 2022/12:45 PM 
 * Task:      Main functionality of CNC machine
 *********************************************************************************)


PROGRAM _INIT

	MpCnc3Axis_0.Enable := TRUE;
	MpCnc3Axis_0.Override := 100.0;	
	gPrintLetter := 'Letter_R';
	
	(* Setting default parameters to all axes *)
	FOR i := 0 TO 2 DO 
		MpAxisBasic_0[i].Enable := TRUE;
		AxisParameters[i].Position := 100.0;
		AxisParameters[i].Velocity := 100.0;
		AxisParameters[i].Acceleration := 300.0;
		AxisParameters[i].Deceleration := 300.0;		
	END_FOR;
	
	MpAxisBasic_Spindle.Enable := TRUE;
	AxisParameters_Spindle.Velocity := 100.0;
	AxisParameters_Spindle.Acceleration := 150.0;
	AxisParameters_Spindle.Deceleration := 150.0;	
	
	(* Setting MpLink of all axes *)
	MpAxisBasic_0[0].MpLink     := ADR(gAxisQX);
	MpAxisBasic_0[1].MpLink     := ADR(gAxisQY);
	MpAxisBasic_0[2].MpLink     := ADR(gAxisQZ);
	MpAxisBasic_Spindle.MpLink 	:= ADR(gAxisQS);
    	
	(* Homing mode change *)
	AxisParameters[2].Homing.Mode		:= mcHOMING_DEFAULT;
	
END_PROGRAM


PROGRAM _CYCLIC
    
	(* Mapping of variables necessary for the visualization *)
	gMotorParametersX.Position			:= 	MpCnc3Axis_0.X;
	gMotorParametersY.Position			:= 	MpCnc3Axis_0.Y;
	gMotorParametersZ.Position			:= 	MpCnc3Axis_0.Z;
	gMotorParametersX.Velocity			:= 	MpAxisBasic_0[0].Velocity;
	gMotorParametersY.Velocity			:= 	MpAxisBasic_0[1].Velocity;
	gMotorParametersZ.Velocity			:= 	MpAxisBasic_0[2].Velocity;
	gMotorParametersSpindle.Velocity	:= 	MpAxisBasic_Spindle.Velocity;
	gAxisXState 						:= 	MpAxisBasic_0[0].Info.PLCopenState;
	gAxisYState 						:= 	MpAxisBasic_0[1].Info.PLCopenState;
	gAxisZState 						:= 	MpAxisBasic_0[2].Info.PLCopenState;
	gSpindleState						:= 	MpAxisBasic_Spindle.Info.PLCopenState;
	
	(* Manual mode is chosen by user. 
	All axes have to be controlled from Manual Page *)
	IF ManualMode AND NOT MpCnc3Axis_0.MoveProgram AND SetupMode = SETUP_READY THEN
		gMode := 'MANUAL';
		gModeInt := 2;
		WorkMode := WORK_MANUAL;
		ActionAllMotorsOff;
		gPartLength := 0;
	END_IF;
	
	(* Automatic mode is chosen by user. 
	Machine will carry out CNC programs in GCode *)
	IF AutomaticMode 
		AND SetupMode = SETUP_READY 
		AND gMotorParametersZ.Position >= 110 
		AND gMode <> 'AUTOMATIC' THEN
		gMode := 'AUTOMATIC';
		gModeInt := 1;
		WorkMode := WORK_DEFAULT;
		ActionAllMotorsOff;
		AxisParameters[0].Velocity := 100.0;
		gModeAutomaticON := TRUE;
	ELSIF AutomaticMode AND gMotorParametersZ.Position < 110 THEN
		ErrorMode := ERROR_POSITION;
	END_IF;
	
	IF gMode <> 'AUTOMATIC' THEN
		gModeAutomaticON := FALSE;
	END_IF;
	
	(* Advanced Automatic mode is chosen by user. 
	New page is available. You can see all options in "Advanced" Page *)
	IF gMode = 'AUTOMATIC' AND gAdvancedMode THEN
		gMode := 'ADVANCED';
		gModeInt := 3;
		WorkMode := WORK_ADVANCED;
		gModeAdvancedON := TRUE;
		gModeAdvancedOFF := FALSE;
	END_IF;
	
	IF gMode <> 'ADVANCED' THEN 
		gModeAdvancedON := FALSE;
		gModeAdvancedOFF := TRUE;
	END_IF;
	
	IF AdvancedTurnOFF THEN
		gMode := ' ';
		WorkMode := WORK_DEFAULT;
		AdvancedMode := ADVANCED_WAITING;
	END_IF;
	
	(* Variables used in visualization connected with Cyclic Mode *)
	CyclicWorkON := CyclicWork;
	CyclicWorkOFF := NOT CyclicWorkON;
	IF gModeAdvanced = 'CYCLICAL WORK' AND NOT CyclicWork THEN
		AdvancedMode := ADVANCED_WAITING;
		gModeAdvanced := 'WAITING';
	END_IF;
	
	(* Variables used in visualization connected with Engraving Mode *)
	WordEngravingON := WordEngraving;
	WordEngravingOFF := NOT WordEngravingON;
	IF gModeAdvanced = 'WORD ENGRAVING' AND NOT WordEngraving THEN
		AdvancedMode := ADVANCED_WAITING;
		gModeAdvanced := 'WAITING';
	END_IF;
	
	(* EMERGENCY STOP. Only use in cases of emergency.
	Whole machine will be immediately turned off *)
	IF EmergencyStop AND MpCnc3Axis_0.PowerOn  THEN
		ErrorMode := EMERGENCY_STOP;
	END_IF;
	
	(* Stop in automatic mode *)
	IF Stop AND gMode = 'AUTOMATIC' AND ErrorMode = NO_ERROR THEN
		WorkMode := WORK_STOP;
	END_IF;
	
	(* Machine is turned off in a regular way, not emergency *)
	IF TurnOFF THEN
		ActionAllMotorsOff;
		gMode := ' ';
		gPartLength := 0;
		MpCnc3Axis_0.Power := FALSE;
		MpCnc3Axis_0.MoveProgram := FALSE;
	END_IF;
	
	(* Setup of all axis *)
	ActionSetup;
	
	(* Automatic engraving *)
	ActionWork;
	
	(* Errors encountered *)
	ActionError;
	
	(* Function blocks *)
	FOR i := 0 TO 2 DO 
		MpAxisBasic_0[i].Parameters := ADR(AxisParameters[i]);
		MpAxisBasic_0[i]();
	END_FOR;
	
	MpAxisBasic_Spindle();
	
	MpAxisBasic_0[0].MpLink     := ADR(gAxisQX);
	MpAxisBasic_0[1].MpLink     := ADR(gAxisQY);
	MpAxisBasic_0[2].MpLink     := ADR(gAxisQZ);
	MpCnc3Axis_0.MpLink 		:= ADR(gCncXYZ);
	MpCnc3Axis_0.Parameters 	:= ADR(CncParameters);
	MpCnc3Axis_0();
	
	MpAxisBasic_Spindle.MpLink 		:= ADR(gAxisQS);
	MpAxisBasic_Spindle.Parameters	:= ADR(AxisParameters_Spindle);
	
	
	
	(* Program checks if machine is powered on, if not, sets the states accordingly *)
	IF (SetupMode = SETUP_READY) AND
		( NOT MpCnc3Axis_0.PowerOn 
		OR NOT MpAxisBasic_0[0].PowerOn
		OR NOT MpAxisBasic_0[0].PowerOn
		OR NOT MpAxisBasic_0[0].PowerOn) THEN
		SetupMode := SETUP_DEFAULT; 
		WorkMode := WORK_DEFAULT;
	END_IF;
		
	(* Axes Error Check *)
	IF MpCnc3Axis_0.Error = TRUE THEN
		ErrorMode := ERROR_AXES;
	END_IF;
	
	(*Spindle digital output *)
	IF MpAxisBasic_Spindle.Velocity > 0 THEN
		SpindleOutput := TRUE;
	ELSE
		SpindleOutput := FALSE;
	END_IF;
	
	
END_PROGRAM
