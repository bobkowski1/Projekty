
ACTION ActionError: 

	CASE ErrorMode OF
		
		(* Everything is working fine, there are no errors *)
		NO_ERROR:
			gModeError := 'NO ERROR';
			MpCnc3Axis_0.ErrorReset := FALSE;
		
		(* Sensor output was not high when expected *)
		ERROR_SENSOR:
			ActionAllMotorsOff;
			gModeError := 'SENSOR ERROR';
			IF Confirm THEN
				ErrorMode := RECOVERY; 
			END_IF;
		
		(* An error related to axes occured *)
		ERROR_AXES:
			gModeError := 'AXES ERROR';
			WorkMode:=WORK_DEFAULT;
			SetupMode:=SETUP_DEFAULT;
			gMode := ' ';
			IF Confirm THEN
				ErrorMode := NO_ERROR; 
				MpCnc3Axis_0.ErrorReset := TRUE;
			END_IF;	
			
		(* Emergency button is clicked *)
		EMERGENCY_STOP:
			gModeError := 'EMERGENCY';
			ActionAllMotorsOff;
			MpCnc3Axis_0.MoveProgram := FALSE;
			SetupMode := SETUP_DEFAULT;
			WorkMode := WORK_DEFAULT;
			gMode := ' ';
			MpCnc3Axis_0.Power := FALSE;
			IF Confirm AND NOT EmergencyStop THEN
				ErrorMode := RECOVERY;
			END_IF;
			
		ERROR_PART_SHORT:
			gModeError := 'PART SHORT';
			IF Confirm THEN
				ErrorMode := NO_ERROR;
				gPartLength := 0;
			END_IF;
			
		ERROR_POSITION:
			gModeError := 'POSITION ERROR';
			IF Confirm THEN
				ErrorMode := NO_ERROR;
				gPartLength := 0;
			END_IF;
			
		ERROR_MANUAL:
			gModeError := 'MANUAL ERROR';
			IF Confirm THEN
				ErrorMode := NO_ERROR;
			END_IF;
			
			
		(* Machine automatically returns to its default position *)
		RECOVERY:
			gModeError := 'RECOVERY';
			gPartLength := 0;
			MpCnc3Axis_0.Power := TRUE;	
			IF MpCnc3Axis_0.PowerOn THEN
				CncParameters.ProgramName := 'Recovery';
				MpCnc3Axis_0.MoveProgram := TRUE;
			END_IF;	
			IF MpCnc3Axis_0.MoveDone THEN
				WorkMode:= WORK_DEFAULT;
				SetupMode:= SETUP_READY;
				ErrorMode:= NO_ERROR;
				MpCnc3Axis_0.MoveProgram := FALSE;
			END_IF;	
		
	END_CASE;
	

END_ACTION
