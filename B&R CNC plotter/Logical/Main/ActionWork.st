ACTION ActionWork:
	
	CASE WorkMode OF
			
		(* Setup is not yet finished *)
		WORK_DEFAULT:
			gModeWork := 'DEFAULT';
			AdvancedMode := ADVANCED_WAITING;
			IF SetupMode = SETUP_READY 
				AND ErrorMode = NO_ERROR 
				AND (gMode= 'AUTOMATIC' OR gMode = 'ADVANCED')
				AND gMotorParametersX.Velocity < 10
				AND gMotorParametersY.Velocity < 10
				AND gMotorParametersZ.Velocity < 10 THEN
				WorkMode:= WORK_WAITING;
			END_IF;		
		
			(* Awaiting for the operator to start the program *)
		WORK_WAITING:
			gModeWork := 'WAITING';
			ActionAllMotorsOff;
			
			IF gMotorParametersZ.Position <> 300
				AND gMotorParametersY.Position <> 0 THEN
				ErrorMode := RECOVERY; 
			END_IF;
			
			IF Start = TRUE AND ErrorMode = NO_ERROR THEN
				WorkMode:= WORK_MEASURING;
			END_IF;
			
			(* Machine is waiting for a part to be measured and checks its length *)
		WORK_MEASURING:
			gModeWork := 'MEASURING';
			MpAxisBasic_0[0].MoveVelocity := TRUE;
			IF gPartLength > (gLetterLength + 40) THEN
				WorkMode:= WORK_POSITIONING;
				MpAxisBasic_0[0].MoveVelocity := FALSE;
			ELSIF gPartLength > 0 THEN
				ErrorMode := ERROR_PART_SHORT;
			END_IF;		
			
			(* Machine will be positioned and ready to start a CNC program *)
		WORK_POSITIONING:
			gModeWork := 'POSITIONING';
			AxisParameters[0].Distance := gPartLength*(-1)/2;
			MpAxisBasic_0[0].MoveAdditive := TRUE;
				
			IF MpAxisBasic_0[0].MoveDone THEN
				IF gSensor THEN
					AxisParameters[1].Position := 200;
					AxisParameters[2].Position := 110;
					MpAxisBasic_0[1].MoveAbsolute := TRUE;
					MpAxisBasic_0[2].MoveAbsolute := TRUE;
				ELSE 
					ErrorMode:=ERROR_SENSOR;
				END_IF;
			END_IF; 
				
			IF MpAxisBasic_0[0].MoveDone AND MpAxisBasic_0[1].MoveDone AND MpAxisBasic_0[2].MoveDone   THEN
				ActionAllMotorsOff;
				MpAxisBasic_0[0].Home := FALSE;
				WorkMode:=WORK_CNC_PROGRAM;
			END_IF;
			
			(* GCode CNCProgram will be carried out *)
		WORK_CNC_PROGRAM:
			gModeWork := 'CNC PROGRAM';
			MpAxisBasic_0[0].Home := TRUE;
			IF gMotorParametersX.Position = 0 AND MpAxisBasic_0[0].Info.PLCopenState <> mcAXIS_HOMING  THEN
				CncParameters.ProgramName := gPrintLetter;
				MpCnc3Axis_0.MoveProgram := TRUE;
			END_IF;
			IF MpCnc3Axis_0.MoveDone THEN
				WorkMode:= WORK_FINISH;
				MpCnc3Axis_0.MoveProgram := FALSE;
			END_IF;	
		
			(* Program is finished but sensor output is still high *)
		WORK_FINISH:
			gModeWork := 'FINISH';
			gPartLength := 0;
			MpAxisBasic_0[0].MoveVelocity := TRUE;
			IF NOT gSensor THEN
				IF gMode = 'ADVANCED' THEN
					WorkMode:= WORK_ADVANCED;
					CyclesFinished := CyclesFinished +1;
					MpAxisBasic_0[0].Home := FALSE;
				ELSE	
					WorkMode:= WORK_MEASURING;
					MpAxisBasic_0[0].Home := FALSE;
				END_IF;
			END_IF; 
		
			(* Automatic program is stopped *)	
		WORK_STOP:
			gModeWork := 'STOP';
			ActionAllMotorsOff;
			SetupMode := SETUP_READY;
			IF MpAxisBasic_0[0].Velocity = 0 AND MpAxisBasic_0[1].Velocity = 0 AND MpAxisBasic_0[2].Velocity = 0 THEN
				IF MpCnc3Axis_0.MoveProgram = FALSE OR MpCnc3Axis_0.MoveDone THEN
					ErrorMode := RECOVERY;
					WorkMode := WORK_DEFAULT;
					MpCnc3Axis_0.MoveProgram := FALSE;
				END_IF;
			END_IF;
			
			(* All motors will be moved manually *)
		WORK_MANUAL:
			ActionManual;
		
			(*Advanced automatic mode is turned on *)
		WORK_ADVANCED:
			ActionAdvancedWork;
		
	END_CASE;
	
	(* Mapping of measurement state *)
	IF WorkMode = WORK_MEASURING OR AdvancedMode = ADVANCED_MEASURING THEN
		gMeasureON := TRUE;
	ELSE
		gMeasureON := FALSE;
	END_IF;
			
				END_ACTION
