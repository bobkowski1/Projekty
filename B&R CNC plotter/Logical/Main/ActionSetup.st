
ACTION ActionSetup: 
	
	CASE SetupMode OF	
		
		(* Default state, setup isn't started yet *)
		SETUP_DEFAULT:
			gModeSetup := 'DEFAULT';
			MpCnc3Axis_0.Power := FALSE;
			MpCnc3Axis_0.Home := FALSE;
			gMachineON := FALSE;
			gMachineOFF := TRUE;
			ActionAllMotorsOff;
			IF (TurnON AND ErrorMode = NO_ERROR ) 
				OR MpCnc3Axis_0.PowerOn THEN
				SetupMode := SETUP_POWER;
			END_IF;	
		
		(* Machine will be turned on *)
		SETUP_POWER:
			gModeSetup := 'POWER';
			MpCnc3Axis_0.Power := TRUE;	
			IF MpCnc3Axis_0.PowerOn THEN
				SetupMode := SETUP_HOME;
				gMachineON := TRUE;
				gMachineOFF := FALSE;
			END_IF;	
			
		(* Machine will be homed *)
		SETUP_HOME:
			gModeSetup := 'HOME';
			MpCnc3Axis_0.Home := TRUE;	
			IF MpCnc3Axis_0.IsHomed  THEN
				SetupMode := SETUP_READY;
				MpCnc3Axis_0.Home := FALSE;
				MpAxisBasic_0[0].Home := FALSE;
			END_IF;	
			
		SETUP_READY:
			gModeSetup := 'READY';
	END_CASE;
	
	
END_ACTION
