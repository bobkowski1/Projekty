(*********************************************************************************
 * Copyright: B&R Automation
 * Author:    Bartosz Bobkowski
 * Created:   July 29, 2022/12:48 PM 
 * Task:      Program manages all alarms occuring
 *********************************************************************************)


PROGRAM _INIT
	
	AlarmCore.MpLink := ADR(gAlarmXCore);
	AlarmCore.Enable := TRUE;
	
	AlarmHistory.MpLink := ADR(gAlarmXHistory);
	AlarmHistory.Enable := TRUE;
	 
END_PROGRAM

PROGRAM _CYCLIC
	
	(* Sensor Alarm occurs when Sensor output is different than expected *)
	IF (gModeError = 'SENSOR ERROR') AND NOT SensorErrorRegistered THEN
		MpAlarmXSet(gAlarmXCore, 'SensorError');
		SensorErrorRegistered := TRUE;
	ELSIF (gModeError <> 'SENSOR ERROR') THEN
		MpAlarmXReset(gAlarmXCore, 'SensorError');
		SensorErrorRegistered := FALSE;
	END_IF;
	
	(* Part Alarm occurs when the part is too short to engrave a letter on it *)
	IF (gModeError = 'PART SHORT') AND NOT PartShortErrorRegistered THEN
		MpAlarmXSet(gAlarmXCore, 'PartShortError');
		PartShortErrorRegistered := TRUE;
	ELSIF (gModeError <> 'PART SHORT') THEN
		MpAlarmXReset(gAlarmXCore, 'PartShortError');
		PartShortErrorRegistered := FALSE;
	END_IF;
	
	(* Emergency Button is clicked, a critical situation happened. All motors are turned off. *)
	IF (gModeError = 'EMERGENCY') AND NOT EmergencyStopRegistered THEN
		MpAlarmXSet(gAlarmXCore, 'EmergencyStop');
		EmergencyStopRegistered := TRUE;
	ELSIF (gModeError <> 'EMERGENCY') THEN
		MpAlarmXReset(gAlarmXCore, 'EmergencyStop');
		EmergencyStopRegistered := FALSE;
	END_IF;
	
	
	(* An error related to Axes occured. You should check the Logger. *)
	IF (gModeError = 'AXES ERROR') AND NOT AxesErrorRegistered THEN
		MpAlarmXSet(gAlarmXCore, 'AxesError');
		AxesErrorRegistered := TRUE;
	ELSIF (gModeError <> 'AXES ERROR') THEN
		MpAlarmXReset(gAlarmXCore, 'AxesError');
		AxesErrorRegistered := FALSE;
	END_IF;
	
	(* Cannot change to Automatic mode in current position *)
	IF (gModeError = 'POSITION ERROR') AND NOT PositionErrorRegistered THEN
		MpAlarmXSet(gAlarmXCore, 'PositionError');
		PositionErrorRegistered := TRUE;
	ELSIF (gModeError <> 'POSITION ERROR') THEN
		MpAlarmXReset(gAlarmXCore, 'PositionError');
		PositionErrorRegistered := FALSE;
	END_IF;
	
	(* Operator tried to turn ON two axes at once in Manual Mode *)
	IF (gModeError = 'MANUAL ERROR') AND NOT ManualErrorRegistered THEN
		MpAlarmXSet(gAlarmXCore, 'ManualError');
		ManualErrorRegistered := TRUE;
	ELSIF (gModeError <> 'MANUAL ERROR') THEN
		MpAlarmXReset(gAlarmXCore, 'ManualError');
		ManualErrorRegistered := FALSE;
	END_IF;
	
	(* There is no error, Label and Alarm sign are turned OFF *)
	IF gModeError = 'NO ERROR' THEN
		AlarmSign := FALSE;
		AlarmLabel := FALSE;
	ELSE
		AlarmSign := NOT AlarmSign; (* IF Alarm is ON, Sign will change state every second *)
		AlarmLabel := TRUE;
	END_IF;
	
	(* History of alarms is cleared by user *)
	IF ClearHistory = TRUE THEN 
		MpAlarmXClearHistory(gAlarmXHistory);
	END_IF;
	
	(* Function blocks *)
	AlarmCore();
	AlarmHistory();
	 
	
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

