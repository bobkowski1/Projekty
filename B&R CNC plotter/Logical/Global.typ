
TYPE
	gLetterChosen : 
		(
		LETTER_R := 0,
		LETTER_U := 1,
		LETTER_W := 2,
		LETTER_X := 3,
		LETTER_I := 4,
		LETTER_I2 := 5
		);
	gMotorParameters : 	STRUCT 
		JogVelocity : REAL;
		JogNegative : BOOL;
		JogPositive : BOOL;
		Position : LREAL;
		PositionInput : LREAL;
		MoveAbsolute : BOOL;
		MoveVelocity : BOOL;
		VelocityInput : REAL;
		Velocity : REAL;
	END_STRUCT;
END_TYPE
