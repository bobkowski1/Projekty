#!/usr/bin/env python

import rospy
from sbt_srvs.srv import QGCWaypoint, QGCWaypointRequest, QGCWaypointResponse

def Client(req, service):
    
    try:
        resp = service(req)
        print(req)
        return resp

    except rospy.ServiceException as e:
        print("Service call failed: %s"%e)


if __name__ == "__main__":
    print("Awaiting service - Waypoint")
    rospy.wait_for_service('Waypoint')
    Waypoint = rospy.ServiceProxy('Waypoint', QGCWaypoint)
    req = QGCWaypointRequest()
    for i in range(3):
        Client(req, Waypoint)
        req.last_wp_num = req.last_wp_num + 1
        req.last_wp_visit_num = req.last_wp_visit_num + 1

