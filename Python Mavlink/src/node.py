#!/usr/bin/env python

from sbt_srvs.srv import QGCWaypoint,QGCWaypointResponse #QGCWaypointRequest
import rospy
from pymavlink import mavutil

class WaypointServer():

    def __init__(self):
        self.LatitudeList = []
        self.LongtitudeList = []
        self.mav = mavutil.mavlink_connection('udp:' + '127.0.0.1:14551')
        self.got_heartbeat = False
        self.Response = QGCWaypointResponse()

    def await_heartbeat(self):
        self.mav.wait_heartbeat()
        print("HEARTBEAT OK\n")

    def Handle(self, req):
        if not self.got_heartbeat:
            self.await_heartbeat()
            self.got_heartbeat = True

        try:
            # Read Waypoint from airframe
            self.mav.waypoint_request_list_send()
            msg = self.mav.recv_match(type=['MISSION_COUNT'],blocking=True)
            print(msg.count)

            for i in range(msg.count):
                self.mav.waypoint_request_send(i)
                msg = self.mav.recv_match(type=['MISSION_ITEM'],blocking=True)
                print ('Receving waypoint {0}') 
                self.LatitudeList.append(msg.x)
                self.LongtitudeList.append(msg.y)

        except rospy.ROSInterruptException:
            pass

        self.Response.wp_visit_num = req.last_wp_visit_num + 1
        self.Response.wp_coord.lat = self.LatitudeList[self.Response.wp_num]
        self.Response.wp_coord.lon = self.LongtitudeList[self.Response.wp_num]
        self.Response.wp_num += 1

        print(req)
        print(self.Response)

        return self.Response


def main():
    ServerInstance = WaypointServer()
    rospy.init_node('Server')
    print("Ready to receive a waypoint.")
    s = rospy.Service('Waypoint', QGCWaypoint, ServerInstance.Handle)
    rospy.spin()

if __name__ == '__main__':
    main()

