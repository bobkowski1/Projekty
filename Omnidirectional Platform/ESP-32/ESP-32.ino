#include <SimpleModbusMaster.h>
#include "BluetoothSerial.h"
#include <Wire.h>

char Czujniki[8];
String CzujnikiString;

#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
#error Bluetooth is not enabled! Please run `make menuconfig` to and enable it
#endif

#if !defined(CONFIG_BT_SPP_ENABLED)
#error Serial Bluetooth not available or not enabled. It is only available for the ESP32 chip.
#endif

BluetoothSerial SerialBT;

//////////////////// Parametry komunikacji ///////////////////
#define baud 115200
#define timeout 4
#define polling 2 // the scan rate
#define retry_count 1000

//Wyjście logiczne odpowiedzialne za kierunek przepływu danych
#define TxEnablePin 5 

//Ilość wszystkich rejestrów na których będą wykonywane operacje
#define TOTAL_NO_OF_REGISTERS 24



#define LED 2

enum
{
  PACKET1,
  PACKET2,
  PACKET3,
  PACKET4,
  PACKET5,
  PACKET6,
  PACKET7,
  PACKET8,
  PACKET9,
  PACKET10,
  PACKET11,
  PACKET12,
  PACKET13,
  PACKET14,
  PACKET15,
  PACKET16,
  PACKET17,
  PACKET18,
  PACKET19,
  PACKET20,
  TOTAL_NO_OF_PACKETS 
};

// Tablica pakietów danych MODBUS
Packet packets[TOTAL_NO_OF_PACKETS];

// Tablica rejestrów MODBUS
unsigned int regs[TOTAL_NO_OF_REGISTERS];

//Wyłączenie przesyłania niepotrzebnych pakietów
void connectionSpeed() {
    packets[0].connection = 0; packets[1].connection = 0; packets[2].connection = 0; packets[3].connection = 0;
    packets[4].connection = 0; packets[5].connection = 0; packets[6].connection = 0; packets[7].connection = 0;
    packets[8].connection = 0; packets[9].connection = 0;
    packets[10].connection = 0; packets[11].connection = 0; packets[12].connection = 1; packets[13].connection = 1; 
    packets[14].connection = 1; packets[15].connection = 1; packets[16].connection = 0; packets[17].connection = 0; 
    packets[18].connection = 0; packets[19].connection = 0; 
}

//Konwersja danych odebranych przez I2C
void receiveEvent(int howMany)
{
  int i;
  i = 0;
  while(1 < Wire.available())
  {
    char c = Wire.read(); 
    if (i<8) { 
    Czujniki[i] = c;
    i++;
    }
CzujnikiString = atoi(Czujniki);
}
}

void setup()
{

  //Interfejs I2C
  Wire.begin(4);                // join i2c bus with address #4
  Wire.onReceive(receiveEvent); // register event
  
  //Bluetooth
  Serial.begin(115200);
  SerialBT.begin("ESP32test"); //Bluetooth device name
  Serial.println("The device started, now you can pair it with bluetooth!");

  //Część związana z MODBUS-em
  
  // Pierwszy napęd
  modbus_construct(&packets[PACKET17], 1, PRESET_SINGLE_REGISTER, 0x0028, 0x0000, 0);   //Uruchomienie silnika
  modbus_construct(&packets[PACKET1], 1, PRESET_SINGLE_REGISTER, 0x0025, 0x0000, 1);    //Tryb prędkości 
  modbus_construct(&packets[PACKET5], 1, PRESET_SINGLE_REGISTER, 0x0013, 0x0000, 2);    //Przyspieszenie
  modbus_construct(&packets[PACKET9], 1, PRESET_SINGLE_REGISTER, 0x0015, 0x0000, 3);    //Opóźnienie
  modbus_construct(&packets[PACKET13], 1, PRESET_MULTIPLE_REGISTERS, 0x00B4, 2, 4);     //Prędkość

  //Drugi napęd
  modbus_construct(&packets[PACKET18], 2, PRESET_SINGLE_REGISTER, 0x0028, 0x0000, 6);
  modbus_construct(&packets[PACKET2], 2, PRESET_SINGLE_REGISTER, 0x0025, 0x0000, 7);
  modbus_construct(&packets[PACKET6], 2, PRESET_SINGLE_REGISTER, 0x0013, 0x0000, 8);
  modbus_construct(&packets[PACKET10], 2, PRESET_SINGLE_REGISTER, 0x0015, 0x0000, 9);
  modbus_construct(&packets[PACKET14], 2, PRESET_MULTIPLE_REGISTERS, 0x00B4, 2, 10);

  //Trzeci napęd
  modbus_construct(&packets[PACKET19], 3, PRESET_SINGLE_REGISTER, 0x0028, 0x0000, 12);
  modbus_construct(&packets[PACKET3], 3, PRESET_SINGLE_REGISTER, 0x0025, 0x0000, 13);
  modbus_construct(&packets[PACKET7], 3, PRESET_SINGLE_REGISTER, 0x0013, 0x0000, 14);
  modbus_construct(&packets[PACKET11], 3, PRESET_SINGLE_REGISTER, 0x0015, 0x0000, 15);
  modbus_construct(&packets[PACKET15], 3, PRESET_MULTIPLE_REGISTERS, 0x00B4, 2, 16);

  
  //Czwarty napęd
  modbus_construct(&packets[PACKET20], 4, PRESET_SINGLE_REGISTER, 0x0028, 0x0000, 18);
  modbus_construct(&packets[PACKET4], 4, PRESET_SINGLE_REGISTER, 0x0025, 0x0000, 19);
  modbus_construct(&packets[PACKET8], 4, PRESET_SINGLE_REGISTER, 0x0013, 0x0000, 20);
  modbus_construct(&packets[PACKET12], 4, PRESET_SINGLE_REGISTER, 0x0015, 0x0000, 21);
  modbus_construct(&packets[PACKET16], 4, PRESET_MULTIPLE_REGISTERS, 0x00B4, 2, 22);
  
  
  // Inicjalizacja maszyny stanowej
  modbus_configure(&Serial2, baud, SERIAL_8N1, timeout, 
  polling, retry_count, TxEnablePin, packets, TOTAL_NO_OF_PACKETS,regs);

  
}

void loop(){

Serial.println(CzujnikiString);

//Odczyt danych z Bluetooth
char Komenda;
static char input[16];
static uint8_t i;
static int KomendaINT = 0;
static int KomendaBufor = 0;


//Konwersja danych na wartość do rejestrów
static unsigned int Speed = 250000; //Prędkość 25 RPM
static unsigned int Speed1 = 3; //Pierwszy rejestr prędkości (ruch do przodu)
static unsigned int Speed2 = 53392; //Drugi rejestr prędkości (ruch do przodu)
static unsigned int Speed3 = 65532; //Pierwszy rejestr prędkości (ruch do tyłu)
static unsigned int Speed4 = 12144; //Drugi rejestr prędkości (ruch do tyłu)

//Bluetooth, komunikacja z użytkownikiem
if (SerialBT.available()) {
  Komenda = SerialBT.read();
  if ( Komenda != '\n' && i < 15 ) 
    input[i++] = Komenda;
else {
    input[i] = '\0';
    i = 0;
    KomendaINT = atoi(input);
  }
  }
 
 //MODBUS
 modbus_update();

//Pierwszy silnik (ustawienie trybu i przyspieszeń)
regs[1] = 0x0001;
regs[2] = 0x0034;
regs[3] = 0x0034;


//Drugi silnik
regs[7] = 0x0001;
regs[8] = 0x0034;
regs[9] = 0x0034;

//Trzeci silnik
regs[13] = 0x0001;
regs[14] = 0x0034;
regs[15] = 0x0034;

//Czwarty silnik
regs[19] = 0x0001;
regs[20] = 0x0034;
regs[21] = 0x0034;


        //Jazda do przodu
        if ( (KomendaINT == 1) && (Czujniki[1] == '1') && (Czujniki[0] == '1') && (Czujniki[3] == '1') ) {
          digitalWrite(2, HIGH);          
          regs[4]  =  Speed3;
          regs[5]  =  Speed4;
          regs[10] =  Speed3;
          regs[11] =  Speed4;
          regs[16] =  Speed1;
          regs[17] =  Speed2;
          regs[22] =  Speed1;
          regs[23] =  Speed2; 
          KomendaBufor = 1;    
          connectionSpeed();
          modbus_update();  
        }

        //Jazda do tyłu
        if ( (KomendaINT == 2) && (Czujniki[2] == '1') && (Czujniki[5] == '1') && (Czujniki[6] == '1') ) {
          digitalWrite(2, HIGH);          
          regs[16] =  Speed3;
          regs[17] =  Speed4;
          regs[22] =  Speed3;
          regs[23] =  Speed4;
          regs[4]  =  Speed1;
          regs[5]  =  Speed2;
          regs[10] =  Speed1;
          regs[11] =  Speed2;
          KomendaBufor = 2;  
          connectionSpeed();
          modbus_update();  
        }

        //Jazda w lewo
        if ( (KomendaINT == 3) && (Czujniki[7] == '1') && (Czujniki[3] == '1') && (Czujniki[2] == '1') ) {
          digitalWrite(2, HIGH);          
          regs[4]  =  Speed3;
          regs[5]  =  Speed4;
          regs[10] =  Speed1;
          regs[11] =  Speed2;
          regs[16] =  Speed3;
          regs[17] =  Speed4;
          regs[22] =  Speed1;
          regs[23] =  Speed2;
          KomendaBufor = 3; 
          connectionSpeed();
          modbus_update();  
        }

        //Jazda w prawo
        if ( (KomendaINT == 4) && (Czujniki[4] == '1') && (Czujniki[0] == '1') && (Czujniki[5] == '1') ) {
          digitalWrite(2, HIGH);     
          regs[4]  = Speed1;
          regs[5]  = Speed2;
          regs[10] = Speed3;
          regs[11] = Speed4;     
          regs[16] = Speed1;
          regs[17] = Speed2;
          regs[22] = Speed3;
          regs[23] = Speed4;
          connectionSpeed();
          KomendaBufor = 4; 
          modbus_update();  
        }

        //Zatrzymanie silników
        if (KomendaINT == 0) {
          digitalWrite(2, LOW);      
          regs[4] =  0x0000;
          regs[5] =  0x0000;
          regs[10] = 0x0000;
          regs[11] = 0x0000;
          regs[16] = 0x0000;
          regs[17] = 0x0000;
          regs[22] = 0x0000;
          regs[23] = 0x0000;
          KomendaBufor = 0; 
          connectionSpeed();
          modbus_update();  
        }

        //Obrót 1
        if (KomendaINT == 7) {
          digitalWrite(2, HIGH);          
          regs[4]  =  Speed1;
          regs[5]  =  Speed2;
          regs[10] =  Speed1;
          regs[11] =  Speed2;
          regs[16] =  Speed1;
          regs[17] =  Speed2;
          regs[22] =  Speed1;
          regs[23] =  Speed2; 
          KomendaBufor = 7;     
          connectionSpeed();
          modbus_update();  
        }

        //Obrót 2
        if (KomendaINT == 8) {
          digitalWrite(2, HIGH);          
          regs[4]  =  Speed3;
          regs[5]  =  Speed4;
          regs[10] =  Speed3;
          regs[11] =  Speed4;
          regs[16] =  Speed3;
          regs[17] =  Speed4;
          regs[22] =  Speed3;
          regs[23] =  Speed4; 
          KomendaBufor = 8;     
          connectionSpeed();
          modbus_update();  
        }

        //Włączenie silników
        if (KomendaINT == 5) {
          digitalWrite(2, LOW); 
          regs[0] = 0x0001 ;  
          regs[6] = 0x0001 ;   
          regs[12] = 0x0001 ;  
          regs[18] = 0x0001 ;   
          packets[0].connection = 1; packets[1].connection = 1; packets[2].connection = 1; packets[3].connection = 1;
          packets[4].connection = 1; packets[5].connection = 1; packets[6].connection = 1; packets[7].connection = 1;
          packets[8].connection = 1; packets[9].connection = 1;
          packets[10].connection = 1; packets[11].connection = 1; packets[12].connection = 1; packets[13].connection = 1; 
          packets[14].connection = 1; packets[15].connection = 1; packets[16].connection = 1; packets[17].connection = 1; 
          packets[18].connection = 1; packets[19].connection = 1; 
          KomendaBufor = 5; 
          modbus_update();  
        }

        
        //Wyłączenie silników
        if (KomendaINT == 6) {
          digitalWrite(2, LOW);   
          regs[0] = 0x0000 ;  
          regs[6] = 0x0000 ;    
          regs[12] = 0x0000 ;  
          regs[18] = 0x0000 ;  
          packets[0].connection = 1; packets[1].connection = 1; packets[2].connection = 1; packets[3].connection = 1;
          packets[4].connection = 1; packets[5].connection = 1; packets[6].connection = 1; packets[7].connection = 1;
          packets[8].connection = 1; packets[9].connection = 1;
          packets[10].connection = 1; packets[11].connection = 1; packets[12].connection = 1; packets[13].connection = 1; 
          packets[14].connection = 1; packets[15].connection = 1; packets[16].connection = 1; packets[17].connection = 1; 
          packets[18].connection = 1; packets[19].connection = 1; 
          KomendaBufor = 6; 
          modbus_update();  
        }

       //Ustawianie prędkości
       if ((KomendaINT >= 20) && (KomendaINT <= 100)) {
          Speed = 0; Speed1 = 0; Speed2 = 0; Speed3 = 0; Speed4 = 0;
          Speed = KomendaINT * 10000;
          Speed1 = Speed / 65536;
          Speed2 = Speed - (Speed1*65536);
          Speed3 = 65536 - Speed1 - 1;
          Speed4 = 65536 - Speed2;
          packets[0].connection = 1; packets[1].connection = 1; packets[2].connection = 1; packets[3].connection = 1;
          packets[4].connection = 1; packets[5].connection = 1; packets[6].connection = 1; packets[7].connection = 1;
          packets[8].connection = 1; packets[9].connection = 1;
          packets[10].connection = 1; packets[11].connection = 1; packets[12].connection = 1; packets[13].connection = 1; 
          packets[14].connection = 1; packets[15].connection = 1; packets[16].connection = 1; packets[17].connection = 1; 
          packets[18].connection = 1; packets[19].connection = 1; 
       }

       //Awaryjne zatrzymanie pojazdu
       if   (   ((Czujniki[1] == '2') || (Czujniki[0] == '2') || (Czujniki[3] == '2')) && (KomendaBufor == 1)
            ||  ((Czujniki[2] == '2') || (Czujniki[5] == '2') || (Czujniki[6] == '2')) && (KomendaBufor == 2)
            ||  ((Czujniki[7] == '2') || (Czujniki[3] == '2') || (Czujniki[2] == '2')) && (KomendaBufor == 3)
            ||  ((Czujniki[4] == '2') || (Czujniki[0] == '2') || (Czujniki[5] == '2')) && (KomendaBufor == 4)
            )
       {
          regs[4]  =  0;
          regs[5]  =  0;
          regs[10] =  0;
          regs[11] =  0;
          regs[16] =  0;
          regs[17] =  0;
          regs[22] =  0;
          regs[23] =  0;  
          connectionSpeed();
          modbus_update();  
       }

modbus_update();        
}
