
#include <Wire.h>        // include Arduino Wire library
#include "Ultrasonic.h"  // include Seeed Studio ultrasonic ranger library

// define ultrasonic ranger data pin
#define RANGERPIN0   0
#define RANGERPIN2   2
#define RANGERPIN3   3
#define RANGERPIN4   4
#define RANGERPIN5   5
#define RANGERPIN6   6
#define RANGERPIN7   7
#define RANGERPIN8   8

String Data;
char Tablica[8];

// initialize ultrasonic library
Ultrasonic ultrasonic0(RANGERPIN0);
Ultrasonic ultrasonic2(RANGERPIN2);
Ultrasonic ultrasonic3(RANGERPIN3);
Ultrasonic ultrasonic4(RANGERPIN4);
Ultrasonic ultrasonic5(RANGERPIN5);
Ultrasonic ultrasonic6(RANGERPIN6);
Ultrasonic ultrasonic7(RANGERPIN7);
Ultrasonic ultrasonic8(RANGERPIN8);

void setup() {
Wire.begin(); // join i2c bus (address optional for master)
}

// main loop
void loop() {

  int centimeters0;
  int centimeters2;
  int centimeters3;
  int centimeters4;
  int centimeters5;
  int centimeters6;
  int centimeters7;
  int centimeters8;


  // get distance in centimeters
  centimeters0 = ultrasonic0.MeasureInCentimeters();
  delay(20);
  centimeters2 = ultrasonic2.MeasureInCentimeters();
  delay(20);
  centimeters3 = ultrasonic3.MeasureInCentimeters();
  delay(20);
  centimeters4 = ultrasonic4.MeasureInCentimeters();
  delay(20);
  centimeters5 = ultrasonic5.MeasureInCentimeters();
  delay(20);
  centimeters6 = ultrasonic6.MeasureInCentimeters();
  delay(20);
  centimeters7 = ultrasonic7.MeasureInCentimeters();
  delay(20);
  centimeters8 = ultrasonic8.MeasureInCentimeters();
  delay(20);
 


  if ( (centimeters0 < 20) && (centimeters0 > 0)) { Tablica[0] = '2'; }
  else { Tablica[0] = '1'; }

  if ( (centimeters2 < 20) && (centimeters2 > 0)) { Tablica[1] = '2'; }
  else { Tablica[1] = '1'; } 

  if ( (centimeters3 < 20) && (centimeters3 > 0)) { Tablica[2] = '2'; }
  else { Tablica[2] = '1'; } 

  if ( (centimeters4 < 20) && (centimeters4 > 0)) { Tablica[3] = '2'; }
  else { Tablica[3] = '1'; } 

  if ( (centimeters5 < 20) && (centimeters5 > 0)) { Tablica[4] = '2'; }
  else { Tablica[4] = '1'; } 

  if ( (centimeters6 < 20) && (centimeters6 > 0)) { Tablica[5] = '2'; }
  else { Tablica[5] = '1'; } 

  if ( (centimeters7 < 20) && (centimeters7 > 0)) { Tablica[6] = '2'; }
  else { Tablica[6] = '1'; } 

  if ( (centimeters8 < 20) && (centimeters8 > 0)) { Tablica[7] = '2'; }
  else { Tablica[7] = '1'; } 
  

  Wire.beginTransmission(4); // transmit to device #4
  Wire.write(Tablica);
  Wire.endTransmission();    // stop transmitting


}
